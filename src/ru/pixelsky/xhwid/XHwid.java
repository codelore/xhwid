/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xhwid;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Smile
 */

public class XHwid extends JavaPlugin implements Listener
{
    public static XHwid plugin;
    
    public static final Logger log = Logger.getLogger("Minecraft");
    
    
    //������
    private FileConfiguration config;
    private File fileConfig;
    
    //MySQL
    public Connection conn = null;
    public Statement statement = null;
    
    public XHwid()
    {
        plugin = this;
    }
    
    @Override
    public void onEnable() 
    {
        loadConfig();
        config = YamlConfiguration.loadConfiguration(fileConfig);
        startConnection();
        
        CommandListener c = new CommandListener(this);
        getCommand("pinfo").setExecutor(c);
        getCommand("banhwid").setExecutor(c);
        getCommand("unbanhwid").setExecutor(c);
        log.info("[] XHwid: Loaded.");
    }
    
    @Override
    public void onDisable()
    {
        try {
            conn.close();
        } catch (SQLException ex) {
            log.info(ex.getSQLState());
        }
    }
    private void loadConfig() {
        fileConfig = new File(getDataFolder(), "config.yml");
        if (!fileConfig.exists()) {
            InputStream resourceAsStream = XHwid.class.getResourceAsStream("/config.yml");
            getDataFolder().mkdirs();
            try {
                FileOutputStream fos = new FileOutputStream(fileConfig);
                byte[] buff = new byte[65536];
                int n;
                while ((n = resourceAsStream.read(buff)) > 0) {
                    fos.write(buff, 0, n);
                    fos.flush();
                }
                fos.close();
                buff = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            log.info("[] XHwid: Config loaded.");
        }
    }
    
    private void startConnection()
    {
        String url = "jdbc:mysql://"+config.getString("url");
        String dbName = config.getString("database");
        String driver = "com.mysql.jdbc.Driver";
        String userName = config.getString("username");
        String password = config.getString("password");
        try 
        {
            System.out.println("[] XHwid: Try connect to the database...");
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url+dbName,userName,password);
            statement = conn.createStatement();
            System.out.println("[] XHwid: Connection succsess.");
        }
        catch (Exception e)
        {
            System.out.println("[] XHwid: Connection to the databese not established.");
            log.info(e.getLocalizedMessage());
        } 
    }
    
    @EventHandler
    public void onJoin(PlayerJoinEvent e)
    {
        ResultSet rs = null;
        final Player p = e.getPlayer();
        String user = null;
        int ban = 0;
        try {
            rs = statement.executeQuery("SELECT * FROM hwid WHERE user = '"+p.getName()+"'");
            if(rs.next())
            {
                user = rs.getString("user");
            }
            else
            {
                p.kickPlayer("����� ������������ �� ���������������!");
            }
            rs = statement.executeQuery("SELECT ban FROM hwid WHERE user = '"+p.getName()+"'");
            while(rs.next())
            {
                ban = rs.getInt("ban");
            }
            if(ban == 1)
            {
                p.kickPlayer("�� ��������!");
            }
        } catch (SQLException ex) {
            log.info("Error: " + ex);
        }
        finally
        {
            try 
            {
                if (rs != null) rs.close();
            } 
            catch (SQLException ex)
            {
                ex.printStackTrace(); 
            }
        }
    }
}
