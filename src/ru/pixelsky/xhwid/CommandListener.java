/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xhwid;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Smile
 */
public class CommandListener implements CommandExecutor 
{
    private XHwid xw;
    
    public CommandListener(XHwid x)
    {
        this.xw = x;
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args)
    {
        if(cmd.getName().equalsIgnoreCase("pinfo"))
        {
            if(!sender.hasPermission("xhwid.pinfo")){
                sender.sendMessage(ChatColor.RED + "� ��� ��� ���� ��� �����!");
                return true;
            }
            ResultSet rs = null;
            if(args.length < 1)
            {
                if(!(sender instanceof Player))
                {
                    sender.sendMessage(ChatColor.RED+"Only for players!");
                    return true;
                }
                String user = null;
                try
                {
                   rs = xw.statement.executeQuery("SELECT user FROM hwid WHERE user='"+sender.getName() +"'");
                   if(rs.next())
                   {
                       user = rs.getString("user");
                   }
                   else
                   {
                       sender.sendMessage(ChatColor.RED +sender.getName() +" not found in database.");
                       return true;
                   }
                   rs = xw.statement.executeQuery("SELECT * FROM hwid WHERE user='"+ sender.getName() +"'");
                   String hwid = null;
                   while(rs.next())
                   {
                       hwid = rs.getString("hwid");
                   }
                   sender.sendMessage("���: " + sender.getName()
                           + "\nIP: " + sender.getServer().getIp()
                           + "\n������: True"
                           + "\nHWID: " + hwid);
                   return true;
                } catch (SQLException ex)
                {
                    xw.log.info("Error: " + ex);
                }
                finally
                {
                    try 
                    {
                        if (rs != null) rs.close();
                    } 
                    catch (SQLException e)
                    {
                        e.printStackTrace(); 
                    }
                }
            }
            else if (args.length == 1)
            {
                String user = null;
                try
                {
                   rs = xw.statement.executeQuery("SELECT user FROM hwid WHERE user='"+args[0]+"'");
                   if(rs.next())
                   {
                       user = rs.getString("user");
                   }
                   else
                   {
                       sender.sendMessage(ChatColor.RED + args[0] +" not found in database.");
                       return true;
                   }
                   String playername = null;
                   String hwid = null;
                   rs = xw.statement.executeQuery("SELECT * FROM hwid WHERE user='"+ args[0] +"'");
                   while(rs.next())
                   {
                       playername = rs.getString("user");
                       hwid = rs.getString("hwid");
                   }
                   Player player = Bukkit.getServer().getPlayer(playername);
                   if(player == null)
                   {
                       sender.sendMessage(
                             "���: " + args[0]
                           + "\n������: False "
                           + "\nHWID: " + hwid
                               );
                       return true;
                   }
                   else
                   {
                        sender.sendMessage(
                             "���: " + args[0]
                           + "\nIP: " + player.getAddress().getHostName()
                           + "\n������: " + player.isOnline()
                           + "\nHWID: " + hwid
                                );
                        return true;
                   }
                } catch (SQLException ex)
                {
                    xw.log.info("Error: " + ex);
                }
                finally
                {
                    try 
                    {
                        if (rs != null) rs.close();
                    } 
                    catch (SQLException e)
                    {
                        e.printStackTrace(); 
                    }
                }
            }
        }
        else if(cmd.getName().equalsIgnoreCase("banhwid"))
        {
            ResultSet rs = null;
            if(!sender.hasPermission("xhwid.banhwid"))
            {
                sender.sendMessage(ChatColor.RED + "� ��� ��� ���� ��� �����!");
                return true;
            }
            if(args.length < 1)
            {
                sender.sendMessage(ChatColor.RED + "�������������: /banhwid <PlayerName>");
                return true;
            }
            else if(args.length == 1)
            {
                String user = null;
                try
                {
                   rs = xw.statement.executeQuery("SELECT user FROM hwid WHERE user='"+ args[0] +"'");
                   if(rs.next())
                   {
                       user = rs.getString("user");
                   }
                   else
                   {
                       sender.sendMessage(ChatColor.RED + args[0] +" not found in database.");
                       return true;
                   }
                   rs = xw.statement.executeQuery("SELECT hwid FROM hwid WHERE user='"+args[0]+"'");
                   String hwid = null;
                   while(rs.next())
                   {
                       hwid = rs.getString("hwid");
                   }
                   xw.statement.executeUpdate("UPDATE hwid SET ban = '1' WHERE hwid='"+ hwid +"'");
                   Player p = Bukkit.getServer().getPlayer(args[0]);
                   if(p != null) p.getServer().banIP(p.getAddress().getHostName());
                   p.kickPlayer("�� ��������.");
                   sender.sendMessage(ChatColor.GREEN + "Banned.");
                   return true;
                }
                catch (SQLException ex)
                {
                    xw.log.info("Error: " + ex);
                }
                finally
                {
                    try 
                    {
                        if (rs != null) rs.close();
                    } 
                    catch (SQLException e)
                    {
                        e.printStackTrace(); 
                    }
                }
            }
        }
        else if(cmd.getName().equalsIgnoreCase("unbanhwid"))
        {
            ResultSet rs = null;
            if(!sender.hasPermission("xhwid.unbanhwid"))
            {
                sender.sendMessage(ChatColor.RED + "� ��� ��� ���� ��� �����!");
                return true;
            }
            if(args.length < 1)
            {
                sender.sendMessage(ChatColor.RED + "�������������: /unbanhwid <PlayerName>");
                return true;
            }
            else if(args.length == 1)
            {
                String user = null;
                try
                {
                   rs = xw.statement.executeQuery("SELECT user FROM hwid WHERE user='"+ args[0] +"'");
                   if(rs.next())
                   {
                       user = rs.getString("user");
                   }
                   else
                   {
                       sender.sendMessage(ChatColor.RED + args[0] +" not found in database.");
                       return true;
                   }
                   xw.statement.executeUpdate("UPDATE hwid SET ban = '0' WHERE user='"+ args[0] +"'");
                   Player p = Bukkit.getServer().getPlayer(args[0]);
                   if(p == null) p.getServer().banIP(p.getAddress().getHostName());
                   sender.sendMessage(ChatColor.GREEN + "Unbanned.");
                   return true;
                }
                catch (SQLException ex)
                {
                    xw.log.info("Error: " + ex);
                }
                finally
                {
                    try 
                    {
                        if (rs != null) rs.close();
                    } 
                    catch (SQLException e)
                    {
                        e.printStackTrace(); 
                    }
                }
            }
        }
        return true;
    }
}
